import React from "react";
import BM from "../BM/BM";
import Radium from "radium";
import Media from "react-media";
import logoGF from "../../img/GF.png";
import { Link } from "react-router-dom";
import "./header.css";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faReact } from "@fortawesome/free-brands-svg-icons";
import { library } from "@fortawesome/fontawesome-svg-core";
import {
	faCheckSquare,
	faPlusCircle,
	faBuilding,
	faCalendar,
	faFileContract,
	faBullseye,
	faBook,
	faGraduationCap,
	faHome,
	faUserGraduate,
	faBriefcase,
} from "@fortawesome/free-solid-svg-icons";

/**
 * Add Icons from "FontAwesome" in the library
 */
library.add(
	faCheckSquare,
	faPlusCircle,
	faBuilding,
	faCalendar,
	faFileContract,
	faBullseye,
	faBook,
	faGraduationCap,
	faReact,
	faHome,
	faUserGraduate,
	faBriefcase,
);

class Header extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			progress: 0,
		};
	}
	scrollToTop() {
		window.scrollTo({
			top: 0,
			behavior: "smooth",
		});
	}
	componentDidMount() {
		const header = document.getElementById("js-header");
		window.onscroll = function () {
			if (
				document.body.scrollTop > 50 ||
				document.documentElement.scrollTop > 50
			) {
				header.classList.add("smaller");
			} else {
				header.classList.remove("smaller");
			}
		};
	}

	render() {
		return (
			<header id="js-header">
				<img
					src={logoGF}
					alt="logo Grégoire Falquerho"
					width="90px"
					style={{ marginBottom: 5 }}
				/>
				<Media queries={{ small: "(max-width: 599px)" }}>
					{(matches) =>
						matches.small ? (
							<span onClick={() => this.scrollToTop()} className="spanEdit">
								<BM />
							</span>
						) : (
							<div style={{ display: "flex", justifyContent: "center" }}>
								<div className="div-menu">
									<Link to="/" className="headerButton" title="Accueil">
										<FontAwesomeIcon icon={faHome} size={"2x"} />
									</Link>

									<Link
										to="/experiences"
										className="headerButton"
										title="Experiences"
									>
										<FontAwesomeIcon icon={faBriefcase} size={"2x"} />
									</Link>

									<Link
										to="/formations"
										className="headerButton"
										title="Formations"
									>
										<FontAwesomeIcon icon={faUserGraduate} size={"2x"} />
									</Link>
								</div>
							</div>
						)
					}
				</Media>
			</header>
		);
	}
}

export default Radium(Header);
