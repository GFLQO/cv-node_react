import React from "react";
import ShowBox from "../ShowBox/ShowBox";
import Radium from "radium";
import { Grid } from "@material-ui/core";
import { Spinner } from "react-bootstrap";
import fadeIn from "react-animations/lib/fade-in";
import Media from "react-media";
import "./experiences.css";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { library } from "@fortawesome/fontawesome-svg-core";
import {
	faCoffee,
	faCog,
	faSpinner,
	faQuoteLeft,
	faSquare,
	faCheckSquare,
	faPlusCircle,
	faBuilding,
	faCalendar,
	faFileContract,
	faBullseye,
} from "@fortawesome/free-solid-svg-icons";

/**
 * Add Icons from "FontAwesome" in the library
 */
library.add(
	faCoffee,
	faCog,
	faSpinner,
	faQuoteLeft,
	faSquare,
	faCheckSquare,
	faPlusCircle,
	faBuilding,
	faCalendar,
	faFileContract,
	faBullseye,
);
/**
 * target URL
 * @type {string}
 */
var targetURL = "https://staging-cv-api.herokuapp.com/";

/**
 * This Experiences.js will presents my experiences
 */
class Experiences extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			experiences: [],
			loading: true,
		};
	}

	/**
	 * Call API
	 * This function will call data's API from the Heroku host and set the state Experiences
	 * @type {function}
	 * @return  {string}            The experiences data retrieved from node Server
	 */
	callAPI() {
		fetch(targetURL)
			.then((res) => res.json())
			.then((res) => this.setState({ experiences: res, loading: false }))
			.catch((err) => err);
	}

	componentDidMount() {
		this.callAPI();
	}

	render() {
		const { experiences, loading } = this.state;
		return (
			<div>
				<div className="u-center-text u-margin-bottom-big u-margin-top-big ">
					<h2 className="heading-secondary" style={styles.fader}>
						Expériences
					</h2>
				</div>
				<Grid
					container
					spacing={0}
					style={{
						display: "flex",
						marginTop: 35,
						justifyContent: "center",
					}}
				>
					{loading ? (
						<Spinner
							animation="border"
							variant="info"
							style={{ margin: "20px" }}
						/>
					) : (
						experiences.map(
							(
								{
									titre,
									technos,
									duree,
									resultats,
									entreprise,
									pole,
									contexte,
								},
								i,
								j,
							) => (
								<React.Fragment>
									<Media queries={{ small: "(max-width: 999px)" }}>
										{(matches) =>
											matches.small ? (
												<Grid
													item
													xs={6}
													style={{
														display: "flex",
														margin: 35,
														justifyContent: "center",
														background:
															"linear-gradient(180deg, rgba(255,255,255,1) 39%, rgba(191,191,194,1) 91%)",
													}}
												>
													<div className="row">
														<div className="col-1-of-3">
															<div className="card">
																<div className="card__side card__side--front-1">
																	<div className="card__title card__title--1">
																		<h4
																			className="card__heading"
																			style={{
																				fontFamily: "Montserrat",
																			}}
																		>
																			{titre}
																		</h4>
																	</div>

																	<div className="card__details">
																		<ul>
																			<li
																				style={{
																					fontFamily: "Poiret One",
																					fontSize: 18,
																				}}
																			>
																				{" "}
																				<FontAwesomeIcon
																					icon={faBuilding}
																					size="2x"
																				/>{" "}
																				: {entreprise}
																			</li>
																			<li>
																				{" "}
																				<FontAwesomeIcon
																					icon={faCalendar}
																					size="2x"
																				/>{" "}
																				: {duree} mois
																			</li>
																			<li>
																				<FontAwesomeIcon
																					icon={faFileContract}
																					size="2x"
																				/>{" "}
																				: {contexte}
																			</li>
																			<li>
																				<FontAwesomeIcon
																					icon={faBullseye}
																					size="2x"
																				/>{" "}
																				: {pole}
																			</li>
																		</ul>
																	</div>
																</div>
																<div className="card__side card__side--back card__side--back-1">
																	<div className="card__cta">
																		<div className="card__price-box">
																			<p className="card__price-only">
																				Détails
																			</p>
																			<p className="card__price-value">
																				{resultats.join(" - ")}
																			</p>
																		</div>
																		<h4
																			style={{ color: "gainsboro", margin: 10 }}
																		>
																			Compétences acquises
																		</h4>
																		{technos.map((tech, j) => (
																			<div
																				key={j}
																				style={{
																					display: "flex",
																					justifyContent: "center",
																					padding: 5,
																				}}
																			>
																				<ShowBox
																					render={({ on, toggle }) => (
																						<div>
																							<div
																								style={{
																									display: "flex",
																									flexDirection: "raw",
																									justifyContent: "center",
																								}}
																							>
																								<h3
																									style={{
																										textDecorationLine:
																											"underline overline",
																										margin: 5,
																										color: "floralwhite",
																									}}
																								>
																									{tech.progiciel}
																								</h3>
																								<FontAwesomeIcon
																									icon={faPlusCircle}
																									color="floralwhite"
																									size="2x"
																									onClick={toggle}
																									style={{ margin: 5 }}
																								/>
																							</div>
																							{on && (
																								<div
																									style={{
																										color: "floralwhite",
																									}}
																								>
																									<ul>
																										<li
																											style={{
																												listStyleType: "none",
																											}}
																										>
																											<li>
																												{tech.competences.join(
																													" - ",
																												)}
																											</li>
																										</li>
																									</ul>
																								</div>
																							)}
																						</div>
																					)}
																				/>
																			</div>
																		))}
																	</div>
																</div>
															</div>
														</div>
													</div>
												</Grid>
											) : (
												<Grid
													item
													xs={3}
													style={{
														display: "flex",
														margin: 35,
														justifyContent: "center",
													}}
												>
													<div className="row">
														<div className="col-1-of-3">
															<div className="card">
																<div className="card__side card__side--front-1">
																	<div className="card__title card__title--1">
																		<h4
																			className="card__heading"
																			style={{
																				fontFamily: "Montserrat",
																			}}
																		>
																			{titre}
																		</h4>
																	</div>

																	<div className="card__details">
																		<ul>
																			<li
																				style={{
																					fontFamily: "Poiret One",
																					fontSize: 18,
																				}}
																			>
																				{" "}
																				<FontAwesomeIcon
																					icon={faBuilding}
																					size="2x"
																				/>{" "}
																				: {entreprise}
																			</li>
																			<li>
																				{" "}
																				<FontAwesomeIcon
																					icon={faCalendar}
																					size="2x"
																				/>{" "}
																				: {duree} mois
																			</li>
																			<li>
																				<FontAwesomeIcon
																					icon={faFileContract}
																					size="2x"
																				/>{" "}
																				: {contexte}
																			</li>
																			<li>
																				<FontAwesomeIcon
																					icon={faBullseye}
																					size="2x"
																				/>{" "}
																				: {pole}
																			</li>
																		</ul>
																	</div>
																</div>
																<div className="card__side card__side--back card__side--back-1">
																	<div className="card__cta">
																		<div className="card__price-box">
																			<p className="card__price-only">
																				Détails
																			</p>
																			<p className="card__price-value">
																				{resultats.join(" - ")}
																			</p>
																		</div>
																		<h4
																			style={{ color: "gainsboro", margin: 10 }}
																		>
																			Compétences acquises
																		</h4>
																		{technos.map((tech, j) => (
																			<div
																				key={j}
																				style={{
																					display: "flex",
																					justifyContent: "center",
																					padding: 5,
																				}}
																			>
																				<ShowBox
																					render={({ on, toggle }) => (
																						<div>
																							<div
																								style={{
																									display: "flex",
																									flexDirection: "raw",
																									justifyContent: "center",
																								}}
																							>
																								<h3
																									style={{
																										textDecorationLine:
																											"underline overline",
																										margin: 5,
																										color: "floralwhite",
																									}}
																								>
																									{tech.progiciel}
																								</h3>
																								<FontAwesomeIcon
																									icon={faPlusCircle}
																									color="floralwhite"
																									size="2x"
																									onClick={toggle}
																									style={{ margin: 5 }}
																								/>
																							</div>
																							{on && (
																								<div
																									style={{
																										color: "floralwhite",
																									}}
																								>
																									<ul>
																										<li
																											style={{
																												listStyleType: "none",
																											}}
																										>
																											<li>
																												{tech.competences.join(
																													" - ",
																												)}
																											</li>
																										</li>
																									</ul>
																								</div>
																							)}
																						</div>
																					)}
																				/>
																			</div>
																		))}
																	</div>
																</div>
															</div>
														</div>
													</div>
												</Grid>
											)
										}
									</Media>
								</React.Fragment>
							),
						)
					)}
				</Grid>
				<hr />
			</div>
		);
	}
}

const styles = {
	fader: {
		animation: "x 2.5s",
		animationName: Radium.keyframes(fadeIn, "fade-in"),
	},
};

export default Radium(Experiences);
