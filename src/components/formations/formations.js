import React from "react";
import "./formations.css";
import fadeIn from "react-animations/lib/fade-in";
import Radium from "radium";
import {
	VerticalTimeline,
	VerticalTimelineElement,
} from "react-vertical-timeline-component";
import "react-vertical-timeline-component/style.min.css";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faReact, faApple } from "@fortawesome/free-brands-svg-icons";
import { library } from "@fortawesome/fontawesome-svg-core";
import {
	faCoffee,
	faCog,
	faSpinner,
	faQuoteLeft,
	faSquare,
	faCheckSquare,
	faPlusCircle,
	faBuilding,
	faCalendar,
	faFileContract,
	faBullseye,
	faBook,
	faGraduationCap,
	faUserTie,
} from "@fortawesome/free-solid-svg-icons";

/**
 * Add Icons from "FontAwesome" in the library
 */
library.add(
	faCoffee,
	faCog,
	faSpinner,
	faQuoteLeft,
	faSquare,
	faCheckSquare,
	faPlusCircle,
	faBuilding,
	faCalendar,
	faFileContract,
	faBullseye,
	faBook,
	faGraduationCap,
	faReact,
);

class Formations extends React.Component {
	render() {
		return (
			<div className="formationscss">
				<div className="u-center-text u-margin-bottom-big u-margin-top-big ">
					<h2 className="heading-secondary" style={styles.fader}>
						Formations
					</h2>
				</div>
				<br />
				<div
					style={{
						background:
							"linear-gradient(180deg, rgba(191,191,194,1) 39%, rgba(255,255,255,1) 91%)",
					}}
				>
					<VerticalTimeline>
						<VerticalTimelineElement
							className="vertical-timeline-element--work"
							contentStyle={{
								background: "rgb(150, 150, 243)",
								color: "#fff",
							}}
							contentArrowStyle={{
								borderRight: "7px solid  rgb(150, 150, 243)",
							}}
							date="2011"
							iconStyle={{ background: "rgb(150, 150, 243)", color: "#fff" }}
							icon={<FontAwesomeIcon icon={"graduation-cap"} size={"2x"} />}
						>
							<h3 className="vertical-timeline-element-title">
								Baccalaureat Littérature
							</h3>
							<h4 className="vertical-timeline-element-subtitle">
								Paris, France
							</h4>
							<p> Institut du Marais - Charlemagne - Pollès</p>
						</VerticalTimelineElement>
						<VerticalTimelineElement
							className="vertical-timeline-element--work"
							date="2012 - 2016"
							contentArrowStyle={{
								borderRight: "7px solid  rgb(150, 150, 243)",
							}}
							contentStyle={{
								background: "rgb(150, 150, 243)",
								color: "#fff",
							}}
							iconStyle={{ background: "rgb(150, 150, 243)", color: "#fff" }}
							icon={<FontAwesomeIcon icon={"graduation-cap"} size={"2x"} />}
						>
							<h3 className="vertical-timeline-element-title">
								Maîtrise en Communication
							</h3>
							<h6>Option Markenting & Publicité</h6>
							<h4 className="vertical-timeline-element-subtitle">
								Paris, France
							</h4>
							<p>Institut Supérieur de Communication (ISCOM Paris)</p>
						</VerticalTimelineElement>
						<VerticalTimelineElement
							className="vertical-timeline-element--education"
							date="2018"
							iconStyle={{
								background: "rgb(233, 30, 99)",
								color: "#fff",
							}}
							icon={<FontAwesomeIcon icon={faApple} size={"2x"} />}
							contentArrowStyle={{
								borderRight: "7px solid  rgb(233, 30, 99)",
							}}
							contentStyle={{
								background: "rgb(233, 30, 99)",
								color: "#fff",
							}}
						>
							<h3 className="vertical-timeline-element-title">
								Formation Développement d'Appications Mobile IOS
							</h3>
							<h4 className="vertical-timeline-element-subtitle">
								3W Academy Paris
							</h4>
							<p>SWIFT, Objective-C, JavaScript </p>
						</VerticalTimelineElement>
						<VerticalTimelineElement
							className="vertical-timeline-element--education"
							date="2019-2020"
							iconStyle={{ background: "rgb(150, 150, 243)", color: "#fff" }}
							contentArrowStyle={{
								borderRight: "7px solid  rgb(150, 150, 243)",
							}}
							contentStyle={{
								background: "rgb(150, 150, 243)",
								color: "#fff",
							}}
							icon={<FontAwesomeIcon icon={faUserTie} size={"2x"} />}
						>
							<h3 className="vertical-timeline-element-title">
								Concepteur / Développeur d'applications
							</h3>
							<h6>Titre R.N.C.P. Niv II</h6>
							<h4 className="vertical-timeline-element-subtitle">
								EPSI Bordeaux
							</h4>
							<p>Alternance @CDiscount</p>
						</VerticalTimelineElement>
						<VerticalTimelineElement
							className="vertical-timeline-element--education"
							date="2019-2020"
							iconStyle={{
								background: "rgb(233, 30, 99)",
								color: "#fff",
							}}
							icon={<FontAwesomeIcon icon={faReact} size={"2x"} />}
							contentArrowStyle={{
								borderRight: "7px solid  rgb(233, 30, 99)",
							}}
							contentStyle={{
								background: "rgb(233, 30, 99)",
								color: "#fff",
							}}
						>
							<h3 className="vertical-timeline-element-title">
								Auto formation ReactJS / React Native
							</h3>
							<h4 className="vertical-timeline-element-subtitle">
								Projets personnels
							</h4>
							<p>Développement Web App & Mobile App </p>
						</VerticalTimelineElement>
					</VerticalTimeline>
				</div>
			</div>
		);
	}
}

const styles = {
	fader: {
		animation: "x 2.5s",
		animationName: Radium.keyframes(fadeIn, "fade-in"),
	},
};

export default Radium(Formations);
