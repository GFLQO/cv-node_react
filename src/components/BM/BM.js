import React from "react";
import { push as Menu } from "react-burger-menu";
import { Link } from "react-router-dom";
import "./BM-styles.css";

class BM extends React.Component {
	
	render() {
		return (
			<Menu noOverlay>
				<li>
					<Link to="/" className="menu-item">
						Home
					</Link>
				</li>
				<br />
				<li>
					<Link to="/experiences" className="menu-item" onClick={() => {}}>
						Expériences
					</Link>
				</li>
				<br />
				<li>
					<Link to="/formations" className="menu-item">
						Formations
					</Link>
				</li>
				<br />
				
			</Menu>
		);
	}
}

export default BM;
