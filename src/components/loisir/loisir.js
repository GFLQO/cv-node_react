import React from "react";
import Lecteur from "../../Youtube/youtube";
import "./loisir.css";
import Radium from "radium";
import Carousel from "nuka-carousel";
import fadeIn from "react-animations/lib/fade-in";

class Loisir extends React.Component {
	render() {
		return (
			<React.Fragment>
				<div className="u-center-text u-margin-bottom-big u-margin-top-big ">
					<h2 className="heading-secondary" style={styles.fader}>
						Loisirs
					</h2>
					<Carousel transitionMode="scroll">
						<div
							style={{
								display: "flex",
								flexDirection: "column",
								justifyContent: "space-around",
								backgroundColor: "black",
								height: "400px",
								width: "100%",
								fontSize: "3em",
								fontFamily: "Poiret One",
							}}
						>
							<h1 style={{ fontSize: "1.5em", color: "white" }}>La Musique</h1>
							<Lecteur />
						</div>
						<div
							style={{
								background:
									"linear-gradient(221deg, rgba(1,6,245,1) 39%, rgba(187,36,36,1) 65%)",
								height: "400px",
								width: "100%",
								fontSize: "3em",
								fontFamily: "Poiret One",
							}}
						>
							{" "}
							<h1
								style={{
									fontSize: "1.5em",
									color: "white",
									paddingTop: "35px",
								}}
							>
								Le Sport
							</h1>
						</div>
						<div
							style={{
								backgroundColor: "#B2B9A7",
								height: "400px",
								width: "100%",
								fontSize: "3em",
								fontFamily: "Poiret One",
							}}
						>
							<h1
								style={{
									fontSize: "1.5em",
									color: "white",
									paddingTop: "35px",
								}}
							>
								L' I.T.
							</h1>
						</div>
					</Carousel>
				</div>
			</React.Fragment>
		);
	}
}
const styles = {
	fader: {
		animation: "x 2.5s",
		animationName: Radium.keyframes(fadeIn, "fade-in"),
		marginBottom: "25px",
	},
};

export default Radium(Loisir);
