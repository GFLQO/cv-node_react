import React from "react";
import Radium from "radium";
import ReactLogo from "../../img/reactlogo1.png";
import NodeLogo from "../../img/nodeJS.png";
import mongoLogo from "../../img/mongodb.png";
import Guitare from "../../img/guitare.jpg";
import fadeIn from "react-animations/lib/fade-in";
import { Link } from "react-router-dom";
import { ProgressBar } from "react-bootstrap";
import "./home.css";
import Formation from "../formations/Formations";
import Experiences from "../experiences/experiences";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

import { library } from "@fortawesome/fontawesome-svg-core";
import {
	faCoffee,
	faCog,
	faCheckSquare,
	faPlusCircle,
	faBuilding,
	faCalendar,
	faFileContract,
	faBullseye,
	faIdCard,
	faGamepad,
	faShoppingCart,
} from "@fortawesome/free-solid-svg-icons";
import { faGitlab } from "@fortawesome/free-brands-svg-icons";

/**
 * Add Icons from "FontAwesome" in the library
 */
library.add(
	faCoffee,
	faCog,
	faCheckSquare,
	faPlusCircle,
	faBuilding,
	faCalendar,
	faFileContract,
	faBullseye,
	faIdCard,
	faGamepad,
	faShoppingCart,
	faGitlab,
);

/**
 * Techno used to build this website
 * @type {string}
 */
var targetURL = "https://staging-cv-api.herokuapp.com/";
class Home extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			progress: 0,
		};
	}

	scrollToTop() {
		window.scrollTo({
			top: 0,
			behavior: "smooth",
		});
	}

	callAPI() {
		fetch(targetURL)
			.then((res) => res.json())
			.then((res) => this.setState({ experiences: res, loading: false }))
			.catch((err) => err);
	}

	componentDidMount() {
		this.callAPI();
		setInterval(() => {
			this.setState({
				progress: this.state.progress + 100,
			});
		}, 1000);
	}
	render() {
		return (
			<>
				<div className="u-center-text u-margin-bottom-big u-margin-top-big ">
					<h2 className="heading-secondary" style={styles.fader}>
						Grégoire Falquerho
					</h2>

					<h4 style={styles.faderItalic}>Concepteur / Développeur Web</h4>
				</div>

				<div className="primarycover">
					<div className="bigger">
						<h1>Bienvenue sur mon site CV </h1>
						<h2>Développeur sur Bordeaux</h2>
						<h5>
							Passionné du Web et des nouvelles technologies depuis des années,
							je me présente aujourd’hui à vous en tant que Concepteur /
							Développeur d'applications.
						</h5>

						<Link to="/Contact" className="buttoncontact">
							CONTACTEZ MOI
						</Link>
					</div>
					<div className="Card">
						<h3>
							Ce site est réalisé avc les techno suivantes :
							<ul>
								<li
									style={{ listStyleType: "none" }}
									onMouseEnter={this.Mouseenter}
								>
									<img src={ReactLogo} width="30px" alt="React logo" /> ReactJS
								</li>
								<li style={{ listStyleType: "none" }}>
									<img src={NodeLogo} width="30px" alt="Node logo" /> NodeJS
								</li>
								<li style={{ listStyleType: "none" }}>
									<img src={mongoLogo} alt="logo mongo db" width="30px" />{" "}
									MongoDB Atlas
								</li>
							</ul>
						</h3>
					</div>
				</div>

				<div className="about-me">
					<div style={styles.seconddiv}>
						<div className="assets">
							<h2 className="titleRev">Mes atouts</h2>
							<ProgressBar
								now={this.state.progress}
								variant="info"
								label="Adaptabilité 100%"
								style={styles.progressBarStyle}
							/>
							<ProgressBar
								now={this.state.progress}
								variant="info"
								label="Travail d'équipe 100%"
								style={styles.progressBarStyle}
							/>
							<ProgressBar
								now={this.state.progress}
								variant="info"
								label="Bonne humeur 	100%"
								style={styles.progressBarStyle}
							/>
							<ProgressBar
								now={this.state.progress}
								variant="info"
								label="Bienveillance 100%"
								style={styles.progressBarStyle}
							/>
						</div>
						<div className="Card">
							<div
								style={{
									display: "flex",
									flexDirection: "column",
								}}
							>
								<h2 style={{ marginBottom: "20px", marginTop: "20px" }}>
									A propos de moi
								</h2>
								<p>
									Reconverti comme développeur d'application (Web & Mobile)
									depuis 2018, j'ai commencé par me former au langage d'Apple
									"SWIFT" avec lequel j'ai pu commencer à appréhender les
									fonctionnements du développement. Par la suite, j'ai eu
									l'occasion de réaliser une année d'alternance sur un poste
									Full-Stack au sein de Cdiscount. J'ai pu y découvrir le
									langage C# sur les framework .NET / .NET Core, ainsi que le
									langage de base de données "T-SQL", le progiciel ETL Talend et
									l'extension Razor pour pouvoir intégrer du C# sur du front.
									Pour parfaire ma formation j'ai décidé de créer un site CV,
									sur lequel vous vous trouvez. Afin de monter en compétence sur
									le frontEnd et découvrir d'autres technos plus récentes, j'ai
									créé une Base de données "no-SQL" sur mongo DB Atlas et j'y ai
									stocké mes "Expériences". Pour les afficher sur la page, j'ai
									créé un backEnd en nodeJS qui est lui hébergé sur Heroku. Le
									backend va donc importer les données de ma BDD sur Atlas
									MongoDB et les renvoyer sur une URL qui sera consommée par la
									page "Expériences" en ReactJS
								</p>
							</div>
						</div>
					</div>
					<div className="project-section">
						<h1 className="h1proj">
							<span> Mes projets </span>
						</h1>
						<div
							className=""
							style={{
								marginTop: "20px",
								display: "flex",
								justifyContent: "space-around",
								color: "white !important",
							}}
						>
							<Link to="/" className="projectLink" onClick={this.scrollToTop}>
								<FontAwesomeIcon
									icon={faIdCard}
									size="6x"
									title="CV en ligne"
								/>
								<h3>this.CV</h3>
							</Link>
							<a
								href="https://heuristic-golick-05dde3.netlify.app"
								target="_blank"
								rel="noopener noreferrer"
								className="projectLink"
							>
								<FontAwesomeIcon
									icon={faGamepad}
									size="6x"
									title="Snake Multiplayer"
								/>
								<h3>Jeu Snake Multiplayer</h3>
							</a>
							<a
								href="https://frosty-ride-a9f4a1.netlify.app/"
								target="_blank"
								rel="noopener noreferrer"
								className="projectLink"
							>
								<FontAwesomeIcon
									icon={faShoppingCart}
									size="6x"
									title="cate produit"
								/>
								<h3>Carte Produit</h3>
							</a>
							<a
								href="https://gitlab.com/GFLQO/cv-node_react"
								target="_blank"
								rel="noopener noreferrer"
								className="projectLink"
							>
								<FontAwesomeIcon
									icon={faGitlab}
									size="6x"
									title="gitlab gflqo"
								/>
								<h3>Mon GitLab</h3>
							</a>
						</div>
						<div
							style={{
								marginTop: "20px",
								display: "flex",
								justifyContent: "center",
								color: "white !important",
							}}
						></div>
					</div>
					<Formation />
					<Experiences />
				</div>
			</>
		);
	}
}

const styles = {
	fader: {
		animation: "x 2.5s",
		animationName: Radium.keyframes(fadeIn, "fade-in"),
	},
	faderItalic: {
		color: "lightgrey",
		fontStyle: "Italic",
		animation: "x 2.5s",
		animationName: Radium.keyframes(fadeIn, "fade-in"),
	},
	progressBarStyle: {
		width: "300px",
		height: "30px",
		margin: "auto",
		marginTop: "10px",
		fontWeight: "bold",
		fontSize: "1em",
	},
	seconddiv: {
		display: "flex",
		justifyContent: "space-around",
		"@media (max-width: 800px)": {
			display: "flex",
			flexDirection: "column",
		},
	},

	bienvenue: {
		alignItems: "center",
		backgroundColor: "lightblue",
	},
};

export default Radium(Home);
