import React from "react";
import Radium from "radium";

import fadeIn from "react-animations/lib/fade-in";

class MentionLegale extends React.Component {
	render() {
		return (
			<div style={{ display: "flex", flexDirection: "column" }}>
				<div className="u-center-text u-margin-bottom-big u-margin-top-big ">
					<h2 className="heading-secondary" style={styles.fader}>
						Mentions Légales
					</h2>
				</div>
				<div>
					<h1 style={{ color: "#ff5757", margin: "20px" }}>
						Informations légales
					</h1>
					<h3>Grégoire Falquerho</h3>
					<p>gregoire.falquerho@gmail.com</p>
					<hr style={{ width: "20%" }} />
					<h1 style={{ color: "#ff5757", margin: "20px" }}>Hebergeur</h1>
					<h3> Netlify </h3>
					<hr style={{ width: "20%" }} />
					<h1 style={{ color: "#ff5757", margin: "20px" }}>Mentions légales</h1>
					<p>
						L’ensemble de ce site relève des législations françaises et
						internationales sur le droit d’auteur et la propriété
						intellectuelle. Tous les droits de reproduction sont réservés, y
						compris pour les documents iconographiques et photographiques.
					</p>
				</div>
			</div>
		);
	}
}

const styles = {
	fader: {
		animation: "x 2.5s",
		animationName: Radium.keyframes(fadeIn, "fade-in"),
	},
};

export default Radium(MentionLegale);
