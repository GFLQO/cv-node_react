import React from "react";
import Radium from "radium";
import fadeIn from "react-animations/lib/fade-in";
import "./contact.css";
import { Spinner } from "react-bootstrap";

var targetURL = "https://staging-cv-api.herokuapp.com/send";

class Contact extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			name: "",
			email: "",
			message: "",
			isLoading: false,
		};
	}

	handleSubmit(e) {
		this.setState({
			isLoading: true,
		});
		e.preventDefault();
		fetch(targetURL, {
			method: "POST",
			body: JSON.stringify(this.state),
			headers: {
				Accept: "application/json",
				"Content-Type": "application/json",
			},
		})
			.then((response) => response.json())
			.then((response) => {
				if (response.status === "success") {
					alert("Message Sent.");
					this.setState({
						isLoading: false,
					});
					this.resetForm();
				} else if (response.status === "fail") {
					alert("Message failed to send.");
					this.setState({
						isLoading: false,
					});
				}
			});
	}

	resetForm() {
		this.setState({ name: "", email: "", message: "" });
	}

	render() {
		return (
			<div className="App">
				<div className="u-center-text u-margin-bottom-big u-margin-top-big ">
					<h2 className="heading-secondary" style={styles.fader}>
						Contact
					</h2>
				</div>
				<form
					id="contact-form"
					onSubmit={this.handleSubmit.bind(this)}
					method="POST"
				>
					<div className="form-group">
						<label htmlFor="name" style={{ fontSize: "12px" }}>
							Nom
						</label>
						<input
							type="text"
							className="form-control"
							value={this.state.name}
							onChange={this.onNameChange.bind(this)}
						/>
					</div>
					<div className="form-group">
						<label htmlFor="exampleInputEmail1" style={{ fontSize: "12px" }}>
							Adresse Email
						</label>
						<input
							type="email"
							className="form-control"
							aria-describedby="emailHelp"
							value={this.state.email}
							onChange={this.onEmailChange.bind(this)}
						/>
					</div>
					<div className="form-group">
						<label htmlFor="message" style={{ fontSize: "12px" }}>
							Message
						</label>
						<textarea
							className="form-control"
							rows="5"
							value={this.state.message}
							onChange={this.onMessageChange.bind(this)}
						/>
					</div>
					<button
						type="submit"
						className="btn btn-primary"
						disabled={this.state.isLoading}
					>
						Submit
					</button>
					{this.state.isLoading && (
						<Spinner
							animation="grow"
							variant="secondary"
							style={{ marginLeft: "20px", marginBottom: "0px" }}
						/>
					)}
				</form>
			</div>
		);
	}

	onNameChange(event) {
		this.setState({ name: event.target.value });
	}

	onEmailChange(event) {
		this.setState({ email: event.target.value });
	}

	onMessageChange(event) {
		this.setState({ message: event.target.value });
	}
}

const styles = {
	fader: {
		animation: "x 2.5s",
		animationName: Radium.keyframes(fadeIn, "fade-in"),
	},
};
export default Radium(Contact);
