import React from "react";
import Home from "./components/home/Home";
import Formations from "./components/formations/Formations";
import Experiences from "./components/experiences/experiences";
import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom";
import Header from "./components/header/Header";
import "bootstrap/dist/css/bootstrap.min.css";
import { Bounce } from "react-animations";
import Radium from "radium";
import Contact from "./components/contactForm/Contact";
import MentLeg from "./components/mentionsLegales/ml";
import "./App.css";

class App extends React.Component {
	render() {
		return (
			<div className="App">
				<Router>
					<Header />
					<div>
						<Switch>
							<Route exact path="/" render={() => <Home />} />
							<Route path="/formations" render={() => <Formations />} />
							<Route path="/experiences" render={() => <Experiences />} />
							<Route path="/contact" render={() => <Contact />} />
							<Route path="/mentionslegales" render={() => <MentLeg />} />
						</Switch>
						<p className="App-intro"></p>
						<div className="footer">
							<div>
								<a className="copyrights"> All right Reserved 2020 | </a>{" "}
								<a>| Grégoire FALQUERHO |</a>
								<Link to="/mentionslegales" style={styles.copyRights}>
									{" "}
									| mentions légales
								</Link>
							</div>
						</div>
					</div>
				</Router>
			</div>
		);
	}
}

const styles = {
	bounce: {
		display: "flex",
		justifyContent: "center",
		marginBottom: "0px",
		animation: "x 1s",
		animationName: Radium.keyframes(Bounce, "bounce"),
	},
	copyRights: {
		padding: -10,
		textDecoration: "none",
		color: "black",
	},
};

export default Radium(App);
