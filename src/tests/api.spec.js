
import { callApi }  from '../components/experiences/experiences'

describe('isXPLoaded', () => {
  it('Experiences est chargé', async () => {
    const result = fetch("https://staging-cv-api.herokuapp.com/")
      .then(res => res.json())
	  .catch(err => err);
      expect(result).not.toBeNull();
      /*
      toEqual([
        {
        "resultats": [
        "J'ai pu, lors de cette première expérience professionnelle, dans une entrerprise aussi respéctée d'un point de vue technologique que CDiscount, acquérir de solide bases en développement web.",
        "Decouverte du developpement web en entreprise",
        "familiarisation avec le poste de Developpeur Full Stack"
        ],
        "_id": "5e961c25917d4e01e4b014ea",
        "id": "1",
        "titre": "Ingénieur d'étude",
        "entreprise": "CDiscount",
        "duree": 12,
        "contexte": "MasterClass / EPSI",
        "pole": "IT Finance / Recouvrement",
        "technos": [
        {
        "competences": [
        "Extract Transform Load Pattern",
        "Filtres Custom",
        " Routines",
        " Fonctions TMap"
        ],
        "id_techno": "1",
        "progiciel": "Talend",
        "niveau": "Intermediaire",
        "domaine": "Intégration de données"
        },
        {
        "competences": [
        "Création de table/Procédures Stockées/ Index",
        "Requêtes",
        " Sous-Requêtes",
        "Manipulation"
        ],
        "id_techno": "2",
        "progiciel": "BDD",
        "niveau": "Junior",
        "domaine": "Gestion de Base de données"
        },
        {
        "competences": [
        ".NET Framework",
        ".NET Core",
        "Persistance des données",
        "API Rest",
        "Razor",
        "MVC"
        ],
        "id_techno": "3",
        "progiciel": "C#",
        "niveau": "Junior",
        "domaine": "Développement Front et Back End"
        }
        ]
        },
        {
        "resultats": [
        "J'ai pu, lors de cette première expérience professionnelle, dans une entrerprise aussi respéctée d'un point de vue technologique que CDiscount, acquérir de solide bases en développement web.",
        "Decouverte du developpement web en entreprise",
        "familiarisation avec le poste de Developpeur Full Stack"
        ],
        "_id": "5e9a04d19c99c739f83f070d",
        "id": "2",
        "titre": "Ingénieur des études générales",
        "entreprise": "cdiiscount",
        "duree": 13,
        "contexte": "MasterClass / EPSI",
        "pole": "IT Finance / Recouvrement",
        "technos": [
        {
        "competences": [
        "Extract Transform Load Pattern",
        "Filtres Custom",
        " Routines",
        " Fonctions TMap"
        ],
        "id_techno": "1",
        "progiciel": "Talend",
        "niveau": "Intermediaire",
        "domaine": "Intégration de données"
        },
        {
        "competences": [
        "Création de table/Procédures Stockées/ Index",
        "Requêtes",
        " Sous-Requêtes",
        "Manipulation"
        ],
        "id_techno": "2",
        "progiciel": "BDD",
        "niveau": "Junior",
        "domaine": "Gestion de Base de données"
        },
        {
        "competences": [
        ".NET Framework",
        ".NET Core",
        "Persistance des données",
        "API Rest",
        "Razor",
        "MVC"
        ],
        "id_techno": "3",
        "progiciel": "C#",
        "niveau": "Junior",
        "domaine": "Développement Front et Back End"
        }
        ]
        }
        ])
        */
    })
  })