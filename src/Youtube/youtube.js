import React from "react";
import Youtube from "react-youtube";
let videoIdList = ["YEKdmH8mtCQ", "iK_tnOOM4kQ"];

class Lecteur extends React.Component {
	constructor(props) {
		super(props);
		this.state = {};
		this.i = 1;
	}
	componentDidMount() {
		this.setState({ videoId: videoIdList[this.i] });
	}
	render() {
		const opts = {
			height: "195",
			width: "320",
			playerVars: {
				// https://developers.google.com/youtube/player_parameters,
				//autoplay: 1,
			},
		};

		return (
			<Youtube
				videoId={this.state.videoId}
				opts={opts}
				onReady={this._onReady}
				onEnd={this._onEnd}
			/>
		);
	}

	_onReady(event) {
		// access to player in all event handlers via event.target
		event.target.pauseVideo();
	}

	_onEnd = () => {
		this.setState({ videoId: videoIdList[++this.i] });
	};
}

export default Lecteur;
// Trouver comment transmettre l'ID de la video pour le mettre en paramtre sur la page loisir nocoops : iK_tnOOM4kQ   / YEKdmH8mtCQ
